// 导入axios库，这是一个基于Promise的HTTP库，可用于浏览器和node.js  
import axios from "axios";  
  
// 定义一个名为upload的函数，它接受两个参数：path和userForm  
function upload(path,userForm){  
  
    // 创建一个新的FormData对象，用于在浏览器中构建一组键值对，这个对象可以被用于XMLHttpRequests发送数据  
    const params = new FormData()  
  
    // 使用for...in循环遍历userForm对象，这个循环会迭代userForm的所有键  
    for (let i in userForm) {  
      // 将userForm对象的每个键值对添加到FormData对象中，键为i，值为userForm[i]  
      params.append(i, userForm[i])  
    }  
  
    // 使用axios库发送一个POST请求到指定的path，同时携带上FormData对象和设置的头部信息  
    // 其中，"Content-Type": "multipart/form-data" 指定了请求的内容类型为多部分表单数据，这是用于文件上传的一种类型  
    return axios.post(path,params, {  
      headers: {  
        "Content-Type": "multipart/form-data"  
      }  
    // 处理服务器响应的Promise，并返回响应的数据部分  
    }).then(res=>res.data)  
}  
  
// 导出一个默认的upload函数，使其可以在其他模块中被导入和使用  
export default upload