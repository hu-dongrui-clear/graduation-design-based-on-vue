// 引入axios库，用于发送http请求  
import axios from "axios";  
  
// 添加一个请求拦截器，在发送请求之前执行某些操作  
axios.interceptors.request.use(  
  // 请求发送之前执行的函数  
  function (config) {  
    // 从本地存储中获取token，并添加到请求头中  
    const token = localStorage.getItem("token");  
    config.headers.Authorization =`Bearer ${token}`;  
    // 返回配置对象，将配置对象传递给下一个拦截器或请求  
    return config;  
  },  
  // 请求错误时执行的函数  
  function (error) {  
    // 返回一个rejected的Promise，以终止请求  
    return Promise.reject(error);  
  }  
);  
  
// 添加一个响应拦截器，在接收到响应后执行某些操作  
axios.interceptors.response.use(  
  // 接收到响应后执行的函数  
  function (response) {  
    // 如果响应头中存在authorization字段，将其存储到本地存储中  
    const { authorization } = response.headers;  
    authorization && localStorage.setItem("token", authorization);  
    // 返回响应对象，将响应对象传递给下一个拦截器或调用handleResponse方法处理响应数据  
    return response;  
  },  
  // 响应错误时执行的函数  
  function (error) {  
    // 如果响应状态码为401，重定向到登录页面  
    const { status} = error.response;  
    if (status === 401) {  
        localStorage.removeItem("token")
        window.location.href="#/login";  
    }  
    // 返回一个rejected的Promise，以终止响应处理  
    return Promise.reject(error);  
  }  
);