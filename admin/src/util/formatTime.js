import moment from "moment/moment" //引入moment时间库
moment.locale("zh-cn");        //设置成中文
const formatTime = {
    getTime:(date)=>{
        return moment(date).format('YYYY/MM/DD'); 
    }
}

export default formatTime