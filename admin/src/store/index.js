// 导入vuex的createStore函数，用于创建vuex store  
import { createStore } from "vuex";  
  
// 导入vuex-persistedstate插件的createPersistedState函数，用于创建持久化状态  
import createPersistedState from "vuex-persistedstate";  
  
// 使用createStore创建一个vuex store  
export default createStore({  
  // 定义state对象，它包含应用程序的当前状态  
  state: {  
    // 判断是否是第一次访问，false表示不是第一次访问，true表示是第一次访问  
    isGetterRouter: false,   
  
    // 控制侧边导航栏的折叠状态，false表示侧边导航栏展开，true表示侧边导航栏折叠  
    isCollapsed: false,   
  
    // 用户的个人信息  
    userInfo: {},  
  },  
  // 定义getters对象，它包含通过getter访问的状态  
  getters: {},  
  // 定义mutations对象，它包含改变状态的方法  
  mutations: {  
    // 定义changeGetterRouter方法，用于改变isGetterRouter的状态  
    changeGetterRouter(state, value) {  
      state.isGetterRouter = value;  
    },  
    // 定义changeCollapsed方法，用于改变isCollapsed的状态，当isCollapsed为true时变为false，反之亦然  
    changeCollapsed(state) {  
      state.isCollapsed = !state.isCollapsed;  
    },  
    // 定义changeUserInfo方法，用于改变userInfo的状态，新的用户信息会合并到当前的userInfo中  
    changeUserInfo(state, value) {  
      state.userInfo = {  
        ...state.userInfo,  
        ...value,  
      };  
    },  
    // 定义clearUserInfo方法，用于清空userInfo的状态  
    clearUserInfo(state,value){  
      state.userInfo = {}  
    }  
  },  
  // 定义actions对象，它包含异步操作和副作用  
  actions: {},  
  // 定义modules对象，用于分割store和代码重用  
  modules: {},  
  // 定义plugins对象，用于添加全局插件和副作用  
  plugins: [  
    // 使用createPersistedState创建一个持久化状态的插件，指定需要持久化的状态路径是isCollapsed和userInfo  
    createPersistedState({  
      paths: ["isCollapsed", "userInfo"],   
    }),  
  ],  
});