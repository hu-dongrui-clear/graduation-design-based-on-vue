import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import ElementPlus from 'element-plus' //elementplus的完全引入
import 'element-plus/dist/index.css'

import '@/util/axios.config' //前端拦截器的使用

createApp(App)
.use(store)
.use(router)
.use(ElementPlus)
.mount('#app')


// const debounce = (fn, delay) => {
//     let timer = null;
//     return function () {
//       let context = this;
//       let args = arguments;
//       clearTimeout(timer);
//       timer = setTimeout(function () {
//         fn.apply(context, args);
//       }, delay);
//     }
//   }
  
//   const _ResizeObserver = window.ResizeObserver;
//   window.ResizeObserver = class ResizeObserver extends _ResizeObserver{
//     constructor(callback) {
//       callback = debounce(callback, 16);
//       super(callback);
//     }
//   }
  