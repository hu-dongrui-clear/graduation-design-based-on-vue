// 引入 jsonwebtoken 库，用于生成和验证 JSON Web Tokens
const jwt = require("jsonwebtoken");

// 定义一个秘钥，用于加密和解密 JSON Web Tokens
const secret = "HuDongRui-anydata";

// 定义一个 JWT 对象，包含两个方法：generate 和 verify
const JWT = {
  // generate 方法用于生成一个 JSON Web Token，接收两个参数：value 和 expires，其中 value 是将要被加密的数据，expires 是 Token 的过期时间
  generate(value, expires) {
    // 使用 jsonwebtoken 库的 sign 方法，将 value 对象和秘钥进行加密，并设置过期时间，返回加密后的 Token
    return jwt.sign(value, secret, { expiresIn: expires });
  },

  // verify 方法用于验证一个 JSON Web Token 是否有效，接收一个参数：token
  verify(token) {
    try {
      // 使用 jsonwebtoken 库的 verify 方法，将 token 和秘钥进行解密，返回解密后的数据对象
      return jwt.verify(token, secret);
    } catch (error) {
      // 如果解密过程中出现错误，说明 token 无效，返回 false
      return false;
    }
  },
};

// 将 JWT 对象导出为模块，以便在其他文件中使用 require 引入和使用该对象中定义的函数
module.exports = JWT;
