var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
const UserRouter = require("./routes/admin/UserRouter");
const JWT = require("./util/JWT");
const NewsRouter = require("./routes/admin/NewsRouter");
const webNewsRouter = require("./routes/web/NewsRouter");
const webProductRouter = require("./routes/web/ProductRouter");
const ProductRouter = require("./routes/admin/ProductRouter");


var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);


app.use(webNewsRouter)
app.use(webProductRouter)

// /adminapi/*.-·后台系统用的
// /webapi/*,-·企业官网用的
// 这是一个使用中间件来验证JWT令牌的Express应用程序。中间件是一种在HTTP请求-响应循环中执行的处理程序。
app.use((req, res, next) => {
  // 如果请求的URL是"/adminapi/user/login"，则跳过身份验证，直接调用next()函数，将控制权交给下一个中间件。
  if (req.url === "/adminapi/user/login") {
    next();
    return;
  }

  // 从HTTP请求头中获取Authorization字段，并提取出JWT令牌。这里假设令牌在Authorization字段的空格后面的部分。
  const token = req.headers["authorization"].split(" ")[1];

  // 如果存在令牌，则验证令牌的有效性。JWT.verify方法会返回令牌的负载（payload），如果令牌有效，则payload是有效的；否则，返回undefined。
  if (token) {
    var payload = JWT.verify(token);
    if (payload) {
      // 如果令牌有效，那么生成一个新的JWT令牌，其中包含相同的负载，并设置有效期为1d
      const newToken = JWT.generate(
        {
          _id: payload._id,
          username: payload.username,
        },
        "1d"
      );
      // 在HTTP响应头中设置新的JWT令牌。
      res.header("Authorization", newToken);
      // 调用next()函数，将控制权交给下一个中间件。
      next();
    } else {
      // 如果令牌无效，则返回HTTP状态码401和错误信息"token过期"。
      res.status(401).send({
        errcode: "-1",
        errorInfo: "token过期",
      });
    }
  } else {
    // 如果没有找到令牌，则返回HTTP状态码401和错误信息"未提供token"。
    res.status(401).send({
      errcode: "-2",
      errorInfo: "未提供token",
    });
  }
});

app.use(UserRouter); //注册调用
app.use(NewsRouter)
app.use(ProductRouter)



// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
