// 引入UserModel模块，这是一个自定义的模型，用于与数据库中的用户数据进行交互  
const UserModel = require("../../models/UserModel")

// 定义UserService对象，它包含了一个登录方法  
const UserService = {
    // login方法，它是异步的，接收一个对象作为参数，该对象中的username和password用于查询用户数据  
    login: async ({ username, password }) => {
        // 使用UserModel的find方法进行查询，查询条件是username和password都匹配  
        // find方法返回的是一个数组，包含所有匹配的用户数据  
        return UserModel.find({
            username,
            password
        })
    },
    upload: async ({ _id, username, introduction, gender, avatar }) => {
        if (avatar) {  //判断是否上传图像 没有上传的话就不更新
            return UserModel.updateOne({
                _id
            }, {
                username, introduction, gender, avatar
            })
        } else {
            return UserModel.updateOne({
                _id
            }, {
                username, introduction, gender
            })
        }
    },
    add: async ({ username, introduction, gender, avatar, password, role }) => {
        return UserModel.create({//Mongoose，它提供了自己的方法来访问MongoDB 数据库，与MongoDB原生驱动程序的insertOne方法不同。如果您要使用Mongoose，可以使用create方法向数据库中插入
            username, introduction, gender, avatar, password, role
        })
    },
    putList:async(body)=>{
        return UserModel.updateOne({_id:body._id},body)
    },
    getList: async ({ id }) => {
        return id ? UserModel.find({ _id: id }, ["username", "role", "password", "introduction"]) : UserModel.find({}, ["username", "role", "avatar", "introduction", "gender"])
    },
    delList: async ({ _id }) => {
        return UserModel.deleteOne({ _id })
    }
}

// 将UserService对象导出，以便在其他文件中使用  
module.exports = UserService