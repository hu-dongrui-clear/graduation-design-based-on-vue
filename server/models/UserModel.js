// 引入 mongoose 模块，它是与 MongoDB 数据库进行交互的 Node.js 库  
const mongoose = require("mongoose")  
  
// 从 mongoose 引入 Schema 对象，它是用于定义 MongoDB 文档结构的类  
const Schema = mongoose.Schema  
  
// 定义一个名为 UserType 的对象，用于描述用户模型的数据结构  
// 这个对象中的每一项都对应数据库中 users 集合中的一个字段  
// ===>users集合代表这个模型对应数据库中的 users 集合  
const UserType = {  
    // 用户名，类型为字符串  
    username: String,  
    // 密码，类型为字符串  
    password: String,  
    // 性别，类型为数字，0 代表女性，1 代表男性  
    gender: Number, // 性别  
    // 个人简介，类型为字符串  
    introduction: String, // 简介  
    // 头像 URL，类型为字符串  
    avatar: String, // 头像  
    // 角色，类型为数字，1 代表管理员，2 代表编辑  
    role: Number, // 管理员1，编辑2  
}  
  
// 使用 mongoose 的 model 方法创建一个名为 UserModel 的模型，该模型对应数据库中的 users 表  
// "user" 是集合的名称，UserType 是集合的结构  
const UserModel = mongoose.model("user", new Schema(UserType)) // 创建一个user模型 和users表一一映射和对应  
  
// 将 UserModel 导出，以便在其他文件中使用  
module.exports = UserModel 