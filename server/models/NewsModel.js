// 引入 mongoose 模块，它是与 MongoDB 数据库进行交互的 Node.js 库  
const mongoose = require("mongoose")  
  
// 从 mongoose 引入 Schema 对象，它是用于定义 MongoDB 文档结构的类  
const Schema = mongoose.Schema  
  
const NewsType = {  
    title:String,
    content:String,
    category:Number,//类别 1  2 3
    cover:String,//封面
    isPublish:Number, //未发布  已发布
    role:Number,//管理员，编辑
    editTime:Date  //编辑时间
}  
  
const NewsModel = mongoose.model("news", new Schema(NewsType)) 
  
// 将 UserModel 导出，以便在其他文件中使用  
module.exports = NewsModel 