var express = require('express');
const UserController = require('../../controllers/admin/UserController');
var UserRouter = express.Router();
//图片上传
const multer  = require('multer')
const upload = multer({ dest: 'public/avataruploads/' }) //图片上传的目标文件夹

/* Post home page. */
UserRouter.post('/adminapi/user/login',UserController.login)//前端发请求到UserRouter判断接口，然后UserRouter再通过此方法跳到UserController
UserRouter.post('/adminapi/user/upload',upload.single('file'),UserController.upload)//更新个人信息
UserRouter.post('/adminapi/user/add',upload.single('file'),UserController.add)//新增个人信息
//用户列表增删改查
UserRouter.get('/adminapi/user/list',UserController.getList)//展示个人信息
UserRouter.get('/adminapi/user/list/:id',UserController.getList)
UserRouter.put('/adminapi/user/list/:id',UserController.putList)
UserRouter.delete('/adminapi/user/list/:id',UserController.delList)//用户管理 删除


module.exports = UserRouter;
